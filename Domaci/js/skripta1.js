$(document).ready(function(){

	//Cuvamo podatke od interesa u promenljivim
	var api_key = 'appid=3726fe9cef4ab9d506ee77d8d8f7f3d1';
	var user_id = "";
	var rest_url = "https://api.openweathermap.org/data/2.5/weather?";

	$("#btnCity").click(function(){

		var find_city = "q="+$("#tbInput").val(); //vrednost input polja je ime grada
		var requestUrl = rest_url+find_city+"&"+api_key;

		console.log(requestUrl);
		
		//ajax get poziv
		//requestUrl - url na koji saljemo zahtev 
		//postaviGrad - callback funkcija koja vrsi obradu odgovora
		//dataType - "json" parametar koji govori o formatu odgovora, potreban zbog cross-origin zahteva
		$.ajax({
		method: "get",
		url: requestUrl,
		dataType: "json",
		success: postaviGrad
		});
		
	});

	$("#btnId").click(function(){
		var find_city = "id="+$("#tbInput").val(); //vrednost input polja je iId grada
		var requestUrl = rest_url+find_city+"&"+api_key;
		console.log(requestUrl);
		
		/*$.get(requestUrl, function(data,status){
			alert(data);
		},"json");*/
		
		$.ajax({
			method: "get",
			url: requestUrl,
			dataType: "json",
			success: postaviGrad
		});
	});
	
	
	$("#btnZip").click(function(){
		var find_city = "zip="+$("#tbInput").val(); //vrednost input polja je iId grada
		var requestUrl = rest_url+find_city+"&"+api_key;
		console.log(requestUrl);
		
		$.ajax({
			method: "get",
			url: requestUrl,
			dataType: "json",
			success: postaviGrad
		});
	});	

	function postaviGrad(data, status){
		//Dobili smo odgovor od servera, dodajemo podatke u DOM
		var prikaz = $("#data1");
		prikaz.empty(); //Ispraznimo prethodni sadrzaj, stavljamo podatke o novom gradu
		if(status == "success"){ //Proveravamo odgovor od servera, da li je zahtev uspesno obradjen
		
			var output="<br/><p>Id grada: " + data.id + "<p>";
			output+="<p>Ime grada: " + data.name + "<p>";
			output+="<p>Oznaka države: " + data.sys.country + "<p>";
			output+="<p>Trenutna temperatura: " + data.main.temp + "<p>";
			output+="<p>Minimalna temperatura: " + data.main.temp_min + "<p>";
			output+="<p>Maksimalna temperatura: " + data.main.temp_max + "<p>";
			output+="<p>Vlažnost vazduha: " + data.main.humidity + "<p>";
			output+="<p>Vazdušni pritisak: " + data.main.pressure + "<p>";
			
			prikaz.html(output);
			
		}else{
			var div = $("<div></div>"); //Ako nije zahtev prosao uspesno, obavestavamo korisnika o tome
			var h1 = $("<h1>Nisam pronasao: "+ $("#tbInput").val() +"</h1>");
			div.append(h1);
			prikaz.append(div);
		}
	};
});